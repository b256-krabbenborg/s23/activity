// console.log("Hello World!")

/*

Create a trainer object using object literals.

Initialize/add the following trainer object properties:
 - Name (String)
 - Age (Number)
 - Pokemon (Array)
 - Friends (Object with Array values for properties)

*/

const trainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
    kanto: ["Brock", "Misty"],
    hoenn: ["May", "Max"],
  },

  // Initialize/add the trainer object method named talk that prints out the message: Pikachu! I choose you!

  talk: function() {
    console.log("Pikachu! I choose you!");
  }
};

console.log(trainer); 

/*Access the trainer object properties using dot and square bracket notation.*/

console.log("Result of dot notation:");
console.log(trainer.name); // Expected Output: Ash Ketchum

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]); // Expected Output: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]

/*Invoke/call the trainer talk object method*/

console.log("Result of talk method:");
trainer.talk(); // Expected Output: Pikachu! I choose you!

/*

Create a constructor for creating a pokemon with the following properties:

- Name (Provided as an argument to the constructor)
- Level (Provided as an argument to the constructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)

*/

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;
  
  /*Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.*/
  
  this.tackle = function(targetPokemon) {
    targetPokemon.health -= this.attack;
    console.log(`${this.name} tackled ${targetPokemon.name}`);
    console.log(`${targetPokemon.name}'s health is now reduced to ${targetPokemon.health}`);

    /*Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.*/

    if (targetPokemon.health -= 0) {
      this.faint(targetPokemon);
    }
  };

  /* Create a faint method that will print out a message: targetPokemon has fainted.*/

  this.faint = function(targetPokemon) {
    console.log(`${targetPokemon.name} has fainted.`);
  };
}

/*Create/instantiate several pokemon objects from the constructor with varying name and level properties.*/

const pikachu = new Pokemon("Pikachu", 12);
const geodude = new Pokemon("Geodude", 8);
const mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

/*Invoke the tackle method of one pokemon object to see if it works as intended.*/

mewtwo.tackle(geodude);

/*

[TEST IN BROWSER CONSOLE]

- invoke the talk method of the trainer

trainer.talk();

- create an instance of the object using the new keyword and passing in the required arguments (name and level) to the constructor

const charizard = new Pokemon("Charizard", 12);

- invoke the tackle method on the created object passing in another Pokemon object as an argument to attack

charizard.tackle(geodude);

*/
